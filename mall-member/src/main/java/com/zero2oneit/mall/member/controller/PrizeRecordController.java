package com.zero2oneit.mall.member.controller;

import com.zero2oneit.mall.common.query.member.PrizeRecordQueryObject;
import com.zero2oneit.mall.common.utils.bootstrap.BoostrapDataGrid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.zero2oneit.mall.member.service.PrizeRecordService;

/**
 * Description:
 *
 * @author Tg
 * @email zero2oneit@163.com
 * @date 2021-05-25
 */
@RestController
@RequestMapping("/admin/member/prizeRecord")
public class PrizeRecordController {

    @Autowired
    private PrizeRecordService prizeRecordService;

    /**
     * 查询抽奖记录列表信息
     * @param qo
     * @return
     */
    @PostMapping("/recordList")
    public BoostrapDataGrid recordList(@RequestBody PrizeRecordQueryObject qo){
        return prizeRecordService.pageList(qo);
    }

}
