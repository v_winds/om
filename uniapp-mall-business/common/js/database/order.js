
let logisticsCom = [
	{
		value: 'yuantong',
		label: '圆通速递'
	},
	{
		value: 'zhongtong',
		label: '中通快递'
	},
	{
		value: 'pingyou',
		label: '中国邮政快递'
	},
	{
		value: 'shunfeng',
		label: '顺丰'
	},
	{
		value: 'yunda',
		label: '韵达'
	},
	{
		value: 'shentong',
		label: '申通'
	},
	{
		value: 'huitong',
		label: '百世快递(原汇通)'
	},
	{
		value: 'tiantian',
		label: '天天快递'
	},
	{
		value: 'debang',
		label: '德邦快递'
	},
];

module.exports = {
	logisticsCom
};