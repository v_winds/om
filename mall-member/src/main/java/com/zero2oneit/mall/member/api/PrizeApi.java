package com.zero2oneit.mall.member.api;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zero2oneit.mall.common.bean.member.MemberAccounts;
import com.zero2oneit.mall.common.bean.member.PrizeInfo;
import com.zero2oneit.mall.common.bean.member.PrizeRule;
import com.zero2oneit.mall.common.utils.R;
import com.zero2oneit.mall.member.dto.PrizeDTO;
import com.zero2oneit.mall.member.service.MemberAccountsService;
import com.zero2oneit.mall.member.service.PrizeRecordService;
import com.zero2oneit.mall.member.service.PrizeRuleService;
import com.zero2oneit.mall.member.service.PrizeWinService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Description:
 *
 * @author Tg
 * @email zero2oneit@163.com
 * @date 2021/5/24
 */
@RestController
@RequestMapping("/api/auth/member/prize")
public class PrizeApi {

    @Autowired
    private PrizeRecordService prizeRecordService;

    @Autowired
    private PrizeRuleService ruleService;

    @Autowired
    private MemberAccountsService accountsService;

    @Autowired
    private PrizeWinService winService;

    @Autowired
    private StringRedisTemplate redisTemplate;

    /**
     * 会员抽奖
     * @param
     * @return
     */
    @PostMapping("/draw")
    public R draw(@RequestBody PrizeDTO prizeDTO){
        return prizeRecordService.draw(prizeDTO);
    }

    /**
     * 加载奖品
     * @param
     * @return
     */
    @PostMapping("/load")
    public R load(@RequestBody PrizeDTO prizeDTO){
        //查询抽奖规则
        QueryWrapper<PrizeRule> wrapper = new QueryWrapper();
        PrizeRule rule = ruleService.getOne(wrapper);
        //查询会员账户信息
        QueryWrapper<MemberAccounts> wrappers = new QueryWrapper();
        wrappers.eq("member_id", prizeDTO.getMemberId());
        MemberAccounts accounts = accountsService.getOne(wrappers);
        //查看会员中奖信息
        List<HashMap<String, Object>> wins = winService.selectList(prizeDTO.getMemberId());

        String jsonStr = redisTemplate.opsForValue().get("om:prizeInfo:list");
        Map data = new HashMap();
        data.put("rule", rule);
        data.put("memberAccount", accounts);
        data.put("prizes", jsonStr);
        data.put("wins", wins);
        return R.ok(data);
    }

}
